import pytest
import torch
from contextlib import nullcontext as does_not_raise


# @pytest.fixture
def names_dataset_v1():
    from candletext.data.dataset import NamesDataset
    train_dataset = NamesDataset(
        path_dataset="tests/docs_test/names_dataset"
    )
    _, val_dataset = torch.utils.data.random_split(train_dataset, [len(train_dataset) - 3000, 3000])

    return train_dataset, val_dataset


# @pytest.fixture
def names_dataset_v2():
    from candletext.data.dataset import NamesDatasetV2
    train_dataset = NamesDatasetV2(
        path_dataset="tests/docs_test/names_dataset"
    )
    _, val_dataset = torch.utils.data.random_split(train_dataset, [len(train_dataset) - 3000, 3000])

    return train_dataset, val_dataset


def names_datamodule(names_dataset):
    from candletext.data.loader import NamesDataLoader
    train_dataset, val_dataset = names_dataset
    datamodule = NamesDataLoader(
        train_dataset=train_dataset,
        val_dataset=val_dataset,
        num_workers=0,
        batch_size=1
    )
    datamodule.setup()
    return datamodule


# def test_setup(names_datamodule):
#     assert names_datamodule.num_categories == 18
#
#
# def test_train_dataloader(names_datamodule):
#     assert len(names_datamodule.train_dataloader()) == 20074
#
#
# def test_training_step(name_analyser, names_datamodule):
#     batch = next(iter(names_datamodule.train_dataloader()))
#     assert isinstance(name_analyser.training_step(batch, 0), torch.Tensor)
#     assert name_analyser.training_step(batch, 0) != 0


@pytest.mark.parametrize('names_dataset, mode', [(names_dataset_v1, 'simple'),
                                                 (names_dataset_v1, 'no_sequence'),
                                                 (names_dataset_v2, 'sequence')])
def test_run_names(names_dataset, mode):
    from pytorch_lightning import Trainer
    from candletext.lightning_models import NamesAnalyser
    with does_not_raise():

        datamodule = names_datamodule(names_dataset())

        name_analyser = NamesAnalyser(
            datamodule=datamodule,
            mode=mode,
            learning_rate=0.001,
            nb_steps=120000
        )

        trainer = Trainer(
            weights_summary=None,
            progress_bar_refresh_rate=10,
            num_sanity_val_steps=0,
            fast_dev_run=10,
            gpus=0,
            max_steps=100,
        )
        trainer.fit(name_analyser, datamodule=datamodule)
