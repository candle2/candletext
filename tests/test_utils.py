def test_unicode_to_ascii():
    from candletext.utils import unicode_to_ascii
    assert unicode_to_ascii('Ślusàrski') == "Slusarski"


def test_read_lines():
    from candletext.utils import read_lines
    assert read_lines("tests/docs_test/Japanese.txt") == ['Abe', 'Abukara', 'Adachi', 'Aida', 'Aihara']



