import torch
import pytorch_lightning as pl
from candletext.data.dataset import NamesDataset, NamesDatasetV2
from candletext.data.loader import NamesDataLoader
from candletext.lightning_models import NamesAnalyser
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning import Trainer


def main():
    train_dataset = NamesDataset(
        path_dataset=r"D:\PycharmProjects\dataset\nlp\names_dataset"
    )

    _, val_dataset = torch.utils.data.random_split(train_dataset, [len(train_dataset) - 3000, 3000])

    datamodule = NamesDataLoader(
        train_dataset=train_dataset,
        val_dataset=val_dataset,
        num_workers=0,
        batch_size=1
    )

    name_analyser = NamesAnalyser(
        datamodule=datamodule,
        mode="no_sequence",
        num_layers=2,
        learning_rate=0.01,
        nb_steps=200000
    )

    logger = TensorBoardLogger(
        save_dir="../../reports/lightning_logs/",
        name="names_test",
        version="no_sequence",
        default_hp_metric=False
    )

    checkpoint_callback = pl.callbacks.ModelCheckpoint(
        dirpath=r"D:\PycharmProjects\candle\candletext\models",
        monitor="Accuracy",
        save_top_k=1,
        mode='max',
        filename=f'names_classifier' + f'_no_sequence' + '_{epoch}',
        save_weights_only=True
    )

    trainer = Trainer(
        weights_summary='top',
        progress_bar_refresh_rate=10,
        num_sanity_val_steps=0,
        gpus=1,
        max_steps=150000,
        logger=logger,
        callbacks=[checkpoint_callback]
    )

    trainer.fit(name_analyser, datamodule=datamodule)


if __name__ == '__main__':
    main()
