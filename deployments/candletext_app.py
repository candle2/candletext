import string
import torch
import streamlit as st
from rnn_no_sequence import RNNnoSequence

NUM_LETTERS = 57
HIDDEN_SIZE = 128
NUM_LAYERS = 2
NUM_CATEGORIES = 18
ALL_LETTERS = string.ascii_letters + " .,;'"

CATEGORIES = ['Arabic', 'Chinese', 'Czech', 'Dutch', 'English', 'French', 'German', 'Greek', 'Irish', 'Italian',
              'Japanese', 'Korean', 'Polish', 'Portuguese', 'Russian', 'Scottish', 'Spanish', 'Vietnamese']


def letter_to_index(letter) -> int:
    """ Find letter index from all_letters, e.g 'a' = 0 """
    return ALL_LETTERS.find(letter)


def line_to_tensor(line) -> torch.Tensor:
    """ Turn a line into a <line_length x 1 x n_letters>, i.e an array of one-hot letter vectors """
    tensor = torch.zeros(len(line), 1, len(ALL_LETTERS))
    for index_letter, letter in enumerate(line):
        tensor[index_letter][0][letter_to_index(letter)] = 1
    return tensor


def _collate_fn(tensor) -> tuple:
    return tuple(zip(*tensor))


def load_names_model(model, checkpoint: str):
    state_dict = torch.load(checkpoint, map_location='cpu')['state_dict']

    try:
        model.load_state_dict(state_dict, strict=True)
        return model
    except RuntimeError:
        for key in list(state_dict.keys()):
            # TODO find a way to generalized the change of layers name (or add new argument)
            # state_dict[key.replace('model.feature_extractor', 'backbone')] = state_dict.pop(key)
            state_dict[key.replace('model.', '')] = state_dict.pop(key)

    model.load_state_dict(state_dict, strict=True)
    return model


st.title("Welcome to candletext")
st.subheader("Try the first names classifier, an IA able to determine your country based on our first name")

model = RNNnoSequence(
    input_size=NUM_LETTERS,
    hidden_size=HIDDEN_SIZE,
    num_layers=NUM_LAYERS,
    output_size=NUM_CATEGORIES
)

hidden_state = model.init_hidden()
model = load_names_model(model, "deployments/names_classifier_no_sequence_epoch=4.ckpt")

input_line = st.text_input("Your name", key="name")
if input_line:

    input_tensor = line_to_tensor(input_line)
    model.eval()
    with torch.no_grad():
        for input_letter in input_tensor:
            output, hidden_state = model(input_letter.unsqueeze(dim=0), hidden_state)


    def category_from_output(output, top_k: int = 3):
        values, indices = output.topk(top_k)
        return [CATEGORIES[i] for i in indices[0]]


    st.markdown(category_from_output(output))
