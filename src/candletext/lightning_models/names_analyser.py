import logging
from typing import Optional

import numpy as np
import torch
from pytorch_lightning import LightningModule, LightningDataModule
from pytorch_lightning.utilities.types import EPOCH_OUTPUT
from torchmetrics import Accuracy

from candletext.models.classification import SimpleRNN, RNNnoSequence, RNNwSequence


class NamesAnalyser(LightningModule):

    def __init__(self,
                 datamodule: LightningDataModule,
                 mode: str = "simple",
                 learning_rate: float = 0.001,
                 n_hidden: int = 128,
                 num_layers: Optional[int] = 2,
                 nb_steps: int = 50000,
                 accuracy_top_k: int = 3):
        super().__init__()
        self.datamodule = datamodule
        self.mode = mode
        self.learning_rate = learning_rate
        self.n_hidden = n_hidden
        self.num_layers = num_layers
        self.nb_steps = nb_steps
        self.n_letters = self.datamodule.num_letters
        self.n_categories = self.datamodule.num_categories
        self.criterion = torch.nn.CrossEntropyLoss()
        self.accuracy = Accuracy(top_k=accuracy_top_k)

        if self.mode == "simple":
            self.model =  self.__build_simple_rnn()

        elif self.mode == "no_sequence":
            self.model = self.__build_pytorch_rnn()

        elif self.mode == "sequence":
            self.model = self.__build_pytorch_rnn_sequence()

        else:
            raise ValueError("The selected mode is not taken care of. Please select of these mode: ['simple', "
                             "'no_sequence', 'sequence'")

    def __build_simple_rnn(self) -> torch.nn.Module:
        return SimpleRNN(self.n_letters, self.n_hidden, self.n_categories)

    def __build_pytorch_rnn(self) -> torch.nn.Module:
        return RNNnoSequence(self.n_letters, self.n_hidden, self.num_layers, self.n_categories)

    def __build_pytorch_rnn_sequence(self) -> torch.nn.Module:
        return RNNwSequence(
            self.n_letters,
            self.n_hidden,
            self.num_layers,
            self.n_categories,
            self.datamodule.sequence_length
        )

    def __run_rnn(self, category_tensor, line_tensor):

        if self.datamodule.batch_size != 1:
            raise ValueError("SimpleRNN works only with batch_size =1 !")

        category_tensor, line_tensor = category_tensor[0], line_tensor[0]
        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        hidden = self.model.init_hidden().to(device)

        for i in range(line_tensor.size()[0]):
            if isinstance(self.model, SimpleRNN):
                output, hidden = self.model(line_tensor[i], hidden)
            else:
                output, hidden = self.model(line_tensor[i].unsqueeze(dim=0), hidden)

        return output, hidden

    def __run_rnn_sequence(self, category_tensor, line_tensor):

        category_tensor, line_tensor = category_tensor[0], line_tensor[0]
        return self.model(line_tensor)

    def category_from_output(self, output):
        top_n, top_i = output.topk(1)
        category_i = top_i[0].item()
        return self.datamodule.all_categories[category_i], category_i

    def training_step(self, batch, batch_idx):
        if batch_idx == 0:
            self.all_losses = []

        category, line, category_tensor, line_tensor = batch

        if self.mode in ["simple", "no_sequence"]:
            output, _ = self.__run_rnn(category_tensor, line_tensor)
        elif self.mode == "sequence":
            output = self.__run_rnn_sequence(category_tensor, line_tensor)
        else:
            logging.error("The selected mode is not taken care of. Please select of these mode: ['simple', "
                          "'no_sequence', 'sequence'")

        loss = self.criterion(output, category_tensor[0])
        self.all_losses.append(loss)

        if batch_idx % 1000 == 0:
            self.log('loss', sum(self.all_losses) / 1000)
            self.all_losses = []

        return loss

    def validation_step(self, batch, batch_idx):
        if batch_idx == 0:
            self.targets, self.predictions = [], []

        category, line, category_tensor, line_tensor = batch

        if self.mode in ["simple", "no_sequence"]:
            output, _ = self.__run_rnn(category_tensor, line_tensor)
        elif self.mode == "sequence":
            output = self.__run_rnn_sequence(category_tensor, line_tensor)
        else:
            logging.error("The selected mode is not taken care of. Please select of these mode: ['simple', "
                          "'no_sequence', 'sequence'")

        _, prediction_index_category = self.category_from_output(output)

        return self.accuracy(output, category_tensor[0])

    def validation_epoch_end(self, outputs: EPOCH_OUTPUT) -> None:
        outputs = [t.cpu().numpy() for t in outputs]
        self.log("Accuracy", np.mean(outputs))

    def configure_optimizers(self):
        parameters = list(self.parameters())
        trainable_parameters = list(filter(lambda p: p.requires_grad, parameters))

        optimizer = torch.optim.SGD(params=trainable_parameters,
                                    lr=self.learning_rate,
                                    )

        lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer=optimizer,
                                                                  T_max=self.nb_steps,
                                                                  eta_min=self.learning_rate / 4)

        return [optimizer], [lr_scheduler]
