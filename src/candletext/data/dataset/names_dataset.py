import string
import torch
from torch.utils.data.dataset import Dataset
from pathlib import Path
from candletext.utils import read_lines, random_list_choice


class NamesDataset(Dataset):
    """ Load the data from the names dataset """

    def __init__(self, path_dataset: str):
        self.path_dataset = Path(path_dataset)
        self.all_letters = string.ascii_letters + " .,;'"
        self.num_letters = len(self.all_letters)
        self.category_lines = {}
        self.categories = []
        self.load_data()

    def load_data(self):
        """ Fill categories and category_lines """
        for filename in self.path_dataset.glob("*.txt"):
            category = filename.stem
            self.categories.append(category)
            lines = read_lines(filename)
            self.category_lines[category] = lines

    def letter_to_index(self, letter) -> int:
        """ Find letter index from all_letters, e.g 'a' = 0 """
        return self.all_letters.find(letter)

    def line_to_tensor(self, line) -> torch.Tensor:
        """ Turn a line into a <line_length x 1 x n_letters>, i.e an array of one-hot letter vectors """
        tensor = torch.zeros(len(line), 1, self.num_letters)
        for index_letter, letter in enumerate(line):
            tensor[index_letter][0][self.letter_to_index(letter)] = 1
        return tensor

    def __len__(self) -> int:
        return sum([len(names) for key, names in self.category_lines.items()])

    def __getitem__(self, item):
        """ Select a random name from the dataset and return its category and tensor """
        category = random_list_choice(self.categories)
        line = random_list_choice(self.category_lines[category])
        category_tensor = torch.tensor([self.categories.index(category)])
        line_tensor = self.line_to_tensor(line)
        return category, line, category_tensor, line_tensor


class NamesDatasetV2(NamesDataset):

    def __init__(self, path_dataset: str):
        super().__init__(path_dataset)
        self.max_name_character = self.get_longest_name()

    def get_longest_name(self):
        """ Get the name with the most of characters """
        max_character = 0
        for filename in self.path_dataset.glob("*.txt"):
            lines = read_lines(filename)
            max_prov_character_name = max(list(map(len, lines)))

            if max_prov_character_name > max_character:
                max_character = max_prov_character_name

        return max_character

    def padding_line_tensor(self, line_tensor: torch.Tensor):
        """ line tensors are inconsistent in their first dimension 'line_length', this method pad the tensor to have
         the following dimension < max_name_character x 1 x num_letters > """
        padding = torch.zeros((self.max_name_character - line_tensor.size(0), 1, self.num_letters))
        return torch.cat((line_tensor, padding), 0)

    def __getitem__(self, item):
        """ Select a random name from the dataset and return its category and tensor """
        category = random_list_choice(self.categories)
        line = random_list_choice(self.category_lines[category])
        category_tensor = torch.tensor([self.categories.index(category)])
        line_tensor = self.padding_line_tensor(self.line_to_tensor(line))
        return category, line, category_tensor, line_tensor