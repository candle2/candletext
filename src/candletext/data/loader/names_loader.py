import torch
from typing import List, Union
from torch.utils.data import DataLoader, Dataset
from pytorch_lightning import LightningDataModule
from candletext.data.dataset import NamesDataset, NamesDatasetV2


class NamesDataLoader(LightningDataModule):

    def __init__(self,
                 train_dataset: NamesDataset = None,
                 val_dataset: NamesDataset = None,
                 num_workers: int = 0,
                 batch_size: int = 2):
        super(NamesDataLoader, self).__init__()
        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.num_workers = num_workers
        self.batch_size = batch_size
        self.num_letters = self.train_dataset.num_letters
        self.num_categories = len(self.train_dataset.categories)
        self.all_categories = self.train_dataset.categories

        if isinstance(train_dataset, NamesDatasetV2):
            self.sequence_length = train_dataset.max_name_character

    @staticmethod
    def _collate_fn(batch: List[torch.Tensor]) -> tuple:
        return tuple(zip(*batch))

    def train_dataloader(self) -> DataLoader:
        """ The Train dataloader """
        return self._data_loader(self.train_dataset, shuffle=True)

    def val_dataloader(self) -> Union[DataLoader, List[DataLoader]]:
        """ define the validation dataloader """
        return self._data_loader(self.val_dataset, shuffle=True)

    def _data_loader(self, dataset: Dataset, shuffle: bool = False) -> DataLoader:
        """ Create DataLoader from Dataset

        :param torch.utils.data.Dataset dataset: the dataset
        :param bool shuffle: if true the dataset will be shuffle
        :return: a dataloader
        """
        return DataLoader(dataset=dataset,
                          batch_size=self.batch_size,
                          shuffle=shuffle,
                          num_workers=self.num_workers,
                          drop_last=False,
                          pin_memory=True,
                          collate_fn=self._collate_fn)
