import torch


class RNNnoSequence(torch.nn.Module):
    """ RNN model for names predictions
    this model has a sequence length of 1
    it processes letter by letter and propagate the hidden state for each letter contains in the name
    """

    def __init__(self, input_size, hidden_size, num_layers, output_size):
        super(RNNnoSequence, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.rnn = torch.nn.RNN(input_size, hidden_size, num_layers, batch_first=True)
        self.fc = torch.nn.Linear(hidden_size * 1, output_size)

    def init_hidden(self):
        return torch.zeros(self.num_layers, 1, self.hidden_size)

    def forward(self, x, hidden):
        # Forward propagate LSTM
        out, hidden = self.rnn(x, hidden)
        out = out.reshape(out.shape[0], -1)

        # Decode the hidden state of the last time step
        out = self.fc(out)

        return out, hidden
