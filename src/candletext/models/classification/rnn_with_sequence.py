import torch


class RNNwSequence(torch.nn.Module):
    """ RNN model for names predictions
    this model has a sequence length of 1
    it processes letter by letter and propagate the hidden state for each letter contains in the name
    """

    def __init__(self, input_size, hidden_size, num_layers, output_size, sequence_length):
        super(RNNwSequence, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.rnn = torch.nn.RNN(input_size, hidden_size, num_layers, batch_first=True)
        self.fc = torch.nn.Linear(hidden_size * sequence_length, output_size)

    def forward(self, x):
        hidden = torch.zeros(self.num_layers, x.size(0), self.hidden_size)
        out, _ = self.rnn(x, hidden)
        out = torch.flatten(out)

        # Decode the hidden state of the last time step
        out = self.fc(out)

        return out.unsqueeze(0)
